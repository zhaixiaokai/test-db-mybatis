package com.personal.datasource.service;

import com.personal.test.service.Test1Service;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestEntry {
    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:spring.xml");
        Test1Service service = (Test1Service)ctx.getBean("test1Service");
        service.getOne();
    }
}
