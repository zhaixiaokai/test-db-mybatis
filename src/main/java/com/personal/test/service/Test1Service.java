package com.personal.test.service;

import com.personal.core.db.dao.Test1TableMapper;
import com.personal.datasource.annoation.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DataSource("dataSourceTest1")
public class Test1Service {
    @Autowired
    Test1TableMapper test1TableMapper;

    @DataSource("dataSourceTest2")
    public void getOne(){
        System.out.println(test1TableMapper.selectByPrimaryKey(2L).getName());
    }
}
